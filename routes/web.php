<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware' => 'locale'], function () {
    Route::get('change-language/{locale}', 'LanguageController@changeLanguage')
        ->name('user.change-language');
    
    Route::get('/global-chat', function (){
        return view('components/global_chat');
    });    
    Route::get('/', function () {
        return view('welcome');
    });
    //Toàn bộ các route khác đặt ở đây.
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
