<?php

return [
    'welcome' => 'Chào mừng bạn!',
    'login' => 'Đăng nhập',
    'logout' => 'Đăng xuất',
    'reg' => 'Đăng ký',
    'username' => 'Tên người dùng',
    'uName' => 'Tên đầy đủ',
    'mailOrUserName' => 'E-mail hoặc tên người dùng',
    'E-Mail Address' => 'Địa chỉ e-mail',
    'Password' => 'Mật khẩu',
    'Confirm Password' => 'Nhập lại mật khẩu',
    'Forgot Your Password?' => 'Quên mật khẩu?',
    'Remember Me' => 'Ghi nhớ đăng nhập',
];