<?php

return [
    'welcome' => 'Welcome!',
    'login' => 'Login',
    'logout' => 'Logout',
    'reg' => 'Register',
    'username' => 'User name',
    'uName' => 'Full name',
    'mailOrUserName' => 'E-mail or user name',
    'E-Mail Address' => 'E-Mail Address',
    'Password' => 'Password',
    'Confirm Password' => 'Confirm password',
    'Forgot Your Password?' => 'Forgot Your Password?',
    'Remember Me' => 'Remember Me',
];