@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('global_chat.global chat') }}</div>

                <div class="card-body">
                    Welcome to global chat!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection