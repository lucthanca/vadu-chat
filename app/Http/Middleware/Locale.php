<?php

namespace App\Http\Middleware;

use Closure;

class Locale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // get data from session, if not exist return default on config
        $locale = \Session::get('locale', config('app.locale'));

        // change to selected lang
        //config(['app.locale' => $locale]);
        \App::setLocale($locale);
        
        return $next($request);
    }
}
