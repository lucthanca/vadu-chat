<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LanguageController extends Controller
{
    /**
     * set locale of user
     */
    public function changeLanguage($locale) 
    {
        \Session::put('locale', $locale);
        return redirect()->back();
    }
}
